from random import choice, randint, getrandbits

KEYSMASH_SCHEMAS = [
    "dgfhjk",
    "sdfghj",
    "dgfuis",
    "sdbfkj",
    "ghfjkl",
    "asdlkjghf"
]

AFFIXES = [
    "stop",
    "no", # the half life scientist duo
    "please",
    "omg",
    "im dying",
    "im weak",
    "holy shit"
]

def randbool(): return bool(getrandbits(1))

def generate(**kwargs):
    length = kwargs.get("length", randint(5,20))
    caps = kwargs.get("caps", randbool())
    affix = kwargs.get("affix", randbool())
    # init some variables
    smash = ""
    lastchar = ""
    # get a random schema
    schema = choice(KEYSMASH_SCHEMAS)
    # pick randomly from the schema, avoiding dupes
    for _ in range(length):
        char = lastchar
        while char == lastchar:
            char = choice(schema)
        smash += char
        lastchar = char
    # add affix if necessary (50% chance between suffix and prefix)
    if affix:
        if randbool():
            smash += " " + choice(AFFIXES)
        else:
            smash = choice(AFFIXES) + " " + smash
    # set to allcaps if desired and return
    return smash.upper() if caps else smash

