#!/usr/bin/env python3

from keysmash import generate
from mastodon import Mastodon
from json import load
with open("config.json") as cfg_file: config = load(cfg_file)

if __name__ == "__main__":
    bot = Mastodon(
        api_base_url = config.get("instanceURL"),
        access_token = config.get("token")
    )
    bot.toot(generate())
