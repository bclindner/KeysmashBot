# KeysmashBot

Based on
[a true story](https://mastodon.technology/@bclindner/104520874677890822).

I decided to document this a bit as it serves as a pretty good template for
"dead simple Mastodon bot you can set up in 30 minutes for a stupid idea".
All you need is a little code chops and an old laptop to repurpose into a home
server and you have something that can do this.

KeysmashBot is currently hosted on [botsin.space](https://botsin.space/@KeysmashBot).

# Running

This assumes you're running Linux (or something with a an sh-style terminal and
cron) and know a little bit about the terminal and how to use Mastodon.

Clone this repository, then in a terminal set up a venv and install the
dependencies (just Mastodon.py):

```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

Start a bot account, go into Preferences > Development, click
New Application, give it the write:statuses scope and set the application name
and URL how you please, then save it. Then, using the access token that process
gives you, create a `config.json` file in this folder that looks like this:

```json
{
	"instanceURL": "https://yourinstance.website",
	"token": "<YOUR_ACCESS_TOKEN_HERE>"
}
```

The `post.py` file has to be run periodically, so I recommend opening up your
terminal and setting up a cronjob:

```sh
crontab -e
```

The bot posts hourly, so something like this would work. You will have to use cd
here; otherwise it cannot find the config.json we made.
```crontab
0 * * * * cd /path/to/keysmashbot; venv/bin/python post.py
```
